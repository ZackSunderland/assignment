//
//  ViewController.h
//  Assignment 2
//
//  Created by Zackary Sunderland on 1/21/15.
//  Copyright (c) 2015 Zackary Sunderland. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>
{
    UILabel* player1Lbl;
    UILabel* player2Lbl;
    
    UIStepper* player2Stepper;
    UIStepper* player1Stepper;
}



@end

