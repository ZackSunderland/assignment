//
//  ViewController.m
//  Assignment 2
//
//  Created by Zackary Sunderland on 1/21/15.
//  Copyright (c) 2015 Zackary Sunderland. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITextField* player1Txt = [[UITextField alloc]initWithFrame:CGRectMake(0, 50, self.view.frame.size.width/2, 100)];
    player1Txt.placeholder = @"Player 1 Name";
    player1Txt.textAlignment = NSTextAlignmentCenter;
    player1Txt.delegate = self;
    [self.view addSubview:player1Txt];    
    
    player1Lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width/2, 100)];
    player1Lbl.text = [NSString stringWithFormat:@"%d", 0];
    player1Lbl.font = [UIFont systemFontOfSize:50];
    player1Lbl.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:player1Lbl];
    
    player1Stepper = [[UIStepper alloc]initWithFrame:CGRectMake(player1Lbl.frame.origin.x, player1Lbl.frame.origin.y + player1Lbl.frame.size.height + 100, player1Lbl.frame.size.width, 50)];
    player1Stepper.center = CGPointMake(player1Lbl.center.x, player1Stepper.center.y);
    [player1Stepper addTarget:self action:@selector(player1StepperTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:player1Stepper];
    
    UITextField* player2Txt = [[UITextField alloc]initWithFrame:CGRectMake(200, 50, self.view.frame.size.width/2, 100)];
    player2Txt.placeholder = @"Player 1 Name";
    player2Txt.textAlignment = NSTextAlignmentCenter;
    player2Txt.delegate = self;
    [self.view addSubview:player2Txt];
    
    player2Lbl = [[UILabel alloc]initWithFrame:CGRectMake(200, 100, self.view.frame.size.width/2, 100)];
    player2Lbl.text = [NSString stringWithFormat:@"%d", 0];
    player2Lbl.font = [UIFont systemFontOfSize:50];
    player2Lbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:player2Lbl];
    

    player2Stepper = [[UIStepper alloc]initWithFrame:CGRectMake(player2Lbl.frame.origin.x, player2Lbl.frame.origin.y + player2Lbl.frame.size.height + 100, player2Lbl.frame.size.width, 50)];
    player2Stepper.center = CGPointMake(player2Lbl.center.x, player2Stepper.center.y);
    [player2Stepper addTarget:self action:@selector(player2StepperTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:player2Stepper];
    
    UIButton* resetBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    resetBtn.frame = CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50);
    [resetBtn setTitle:@"Reset Scores" forState:UIControlStateNormal];
    [self.view addSubview:resetBtn];
    [resetBtn addTarget:self action:@selector(resetBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)resetBtnTouched:(id)sender {
    player1Lbl.text = @"0";
    player2Lbl.text = @"0";
    
    player1Stepper.value = 0;
    player2Stepper.value = 0;
}

-(void)player2StepperTouched:(UIStepper*)playerStepper {
    player2Lbl.text = [NSString stringWithFormat:@"%.f",playerStepper.value];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)player1StepperTouched:(UIStepper*)playerStepper {
    player1Lbl.text = [NSString stringWithFormat:@"%.f",playerStepper.value];
}

@end
